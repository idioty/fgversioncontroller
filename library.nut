class FGVersionController extends GSLibrary {
	function GetAuthor()      { return "idioty"; }
	function GetName()        { return "FGVersionController"; }
	function GetShortName()   { return "FGVC"; }
	function GetDescription() { return "You can easily query the OpenTTD versions"; }
	function GetAPIVersion()  { return "1.2"; }
	function GetVersion()     { return 2; }
	function GetDate()        { return "2013-11-03"; }
	function GetURL()         { return ""; }
	function CreateInstance() { return "FGVersionController"; }
	function GetCategory()    { return "Util"; }
}

RegisterLibrary(FGVersionController());
